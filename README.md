# LCD_I2C_Teensy36 library
This is library for I2C display. It wokrs with Teensy 3.6.

# Installation
1. Go to [releases](https://gitlab.com/hypercode/Teensy-36-i2c-lcd/releases/)
1. On latest release click "Source code" and then select download ZIP 
1. Save file to prefered location 
1. Go to Arduino IDE and selectg *Sketch > Include library > Add ZIP library*
1. Select peviously saved .zip file


# Usage
To use the library in your own sketch, select it from *Sketch > Import Library*.
### Electrical connections
SDA and SCL pins require pullup resistor to 3V3 (around 5K ohm)

| I2C LCD module | Teesy 3.6 |
| ------ | ------ |
| VCC | VCC +3V3 |
| GND | GND | 
| SCL | 19 (aka A5) | 
| SDA | 18 (aka A4) | 

![Schematics](docs/images/cropped.png)

### Basic example
Another basic example can be found in Arduino IDE: File/Examples/LCD_I2C_Teensy36/HelloWorld

```c++
#include <LCD_I2C_Teensy36.h>

LCD_I2C_Teensy36 lcd(0x27, 16, 2); //Set LCD to 16chars and 2 lines

void setup() {

  lcd.begin(); //init display 
  lcd.backlight(); //light up backlight

  
  lcd.clearhome(); //Clear display and return to home
  lcd.print("Hello world!");//Print characters on display
  delay(5000);
}

void loop(){

  lcd.clear();
  lcd.setCursor(5, 0); //set cursor to 5th character on first line
  lcd.print("Sup!");
  delay(1000);

  lcd.clear(); //clear tge display
  lcd.setCursor(0, 1); //set cursor to second line
  lcd.print("Sup!");
  delay(1000);
}.

```

# Credits

Based on [fdebrabander's library](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library)
